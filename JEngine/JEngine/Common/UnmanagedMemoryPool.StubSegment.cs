﻿using System;
using System.Diagnostics.Contracts;
using System.Runtime.InteropServices;
using System.Threading;

namespace JEngine.Common
{
    public sealed partial class UnmanagedMemoryPool
    {
        /// <summary>
        /// Сегмент "заглушка" осуществляет управление буферами используя <see cref="Marshal"/>
        /// </summary>
        internal sealed class StubSegment : IPoolSegment
        {
            private int _refCounter = 0;

            /// <summary>
            /// Выделяет неуправляемый буфер используя <see cref="Marshal.AllocHGlobal"/>
            /// </summary>
            /// <param name="size">Размер буфера в байтах</param>
            /// <param name="align">Требуемое выравнивание буфера в байтах</param>
            /// <param name="buffer">В случае успеха содержит описание буфера, в противном случае значение не определено</param>
            /// <returns>В случае успеха возвращается true, в противном случае false</returns>
            public bool AllocBuffer(int size, int align, out UnmanagedBuffer buffer)
            {
                Contract.Requires(size > 0 && align > 0);
                // TODO Ходят слухи что Marshal.AllocHGlobal выделяет память с выравниванием 8 байт, в теории это позволяет выделять на 8 байт меньше памяти
                var rawPtr = Marshal.AllocHGlobal(size + align);
                buffer = new UnmanagedBuffer(this, rawPtr, rawPtr.Align(align), size);

                Interlocked.Increment(ref _refCounter);

                return true;
            }

            /// <summary>
            /// Освобождает неуправляемый буфер используя <see cref="Marshal.FreeHGlobal"/>
            /// </summary>
            /// <param name="buffer"></param>
            public void FreeBuffer(ref UnmanagedBuffer buffer)
            {
                Contract.Requires(buffer.RawPointer != IntPtr.Zero);
                Marshal.FreeHGlobal(buffer.RawPointer);

                Interlocked.Decrement(ref _refCounter);
            }

            public void Dispose()
            {
                Contract.Requires(_refCounter == 0);
            }
        }
    }
}