using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Runtime.InteropServices;
using System.Threading;

namespace JEngine.Common
{
    public sealed partial class UnmanagedMemoryPool
    {
        /// <summary>
        /// Сегмент памяти пула, осуществляет управление буферами используя подсчет ссылок
        /// </summary>
        internal sealed class MemorySegment : IPoolSegment
        {
            private readonly IntPtr _memoryPtr;
            private readonly int _segmentSize;
            private int _currentOffset = 0;
            private int _refCounter = 0;

            /// <summary>
            /// 
            /// </summary>
            /// <param name="size">Размер сегмента в байтах</param>
            public MemorySegment(int size)
            {
                _memoryPtr = Marshal.AllocHGlobal(size);
                _segmentSize = size;
            }

            ~MemorySegment()
            {
                Marshal.FreeHGlobal(_memoryPtr);
                Debug.Assert(false, "Finalizer call, use Dispose!");
            }

            /// <summary>
            /// Выделяет неуправляемый буфер внутри сегмента
            /// </summary>
            /// <param name="size">Размер буфера в байтах</param>
            /// <param name="align">Требуемое выравнивание буфера в байтах</param>
            /// <param name="buffer">В случае успеха содержит описание буфера, в противном случае значение не определено</param>
            /// <returns>В случае успеха возвращается true, в противном случае false</returns>
            public bool AllocBuffer(int size, int align, out UnmanagedBuffer buffer)
            {
                Contract.Requires(size > 0 && size < _segmentSize);
                int currentOffset, newOffset;
                do
                {
                    currentOffset = Interlocked.CompareExchange(ref _currentOffset, 0, 0);
                    newOffset = currentOffset + size + align;
                    if (newOffset > _segmentSize)
                    {
                        buffer = new UnmanagedBuffer();
                        return false;
                    }
                }
                while (Interlocked.CompareExchange(ref _currentOffset, newOffset, currentOffset) != currentOffset);

                Interlocked.Increment(ref _refCounter);

                var rawPtr = new IntPtr(_memoryPtr.ToInt32() + currentOffset);
                buffer = new UnmanagedBuffer(this, rawPtr, rawPtr.Align(align), size);
                return true;
            }

            /// <summary>
            /// Освобождает неуправляемый буфер внутри сегмента
            /// </summary>
            /// <param name="buffer"></param>
            public void FreeBuffer(ref UnmanagedBuffer buffer)
            {
                Contract.Requires(buffer.Segment == this);

                int currentOffset = _currentOffset;//Interlocked.CompareExchange(ref _currentOffset, 0, 0);
                int refCounter = Interlocked.Decrement(ref _refCounter);

                // Если уничтожена последняя ссылка на сегмент, пытаемся сбросить текущий указатель смещения на начало сегмента
                // если память в текущем сегменте не выделялась другим потокам операция завершится успешно
                if (refCounter == 0)
                {
                    Interlocked.CompareExchange(ref _currentOffset, 0, currentOffset);
                }
            }

            #region IDisposable

            private bool _isDisposed = false;

            public void Dispose()
            {
                if (!_isDisposed)
                {
                    Marshal.FreeHGlobal(_memoryPtr);
                    GC.SuppressFinalize(this);

                    _isDisposed = true;
                }
            }

            #endregion
        }
    }
}