﻿using System;

namespace JEngine.Common
{
    public sealed partial class UnmanagedMemoryPool
    {
        /// <summary>
        /// Интерфейс сегмента пула
        /// </summary>
        internal interface IPoolSegment : IDisposable
        {
            /// <summary>
            /// Выделяет неуправляемый буфер внутри сегмента
            /// </summary>
            /// <param name="size">Размер буфера в байтах</param>
            /// <param name="align">Требуемое выравнивание буфера в байтах</param>
            /// <param name="buffer">В случае успеха содержит описание буфера, в противном случае значение не определено</param>
            /// <returns>В случае успеха возвращается true, в противном случае false</returns>
            bool AllocBuffer(int size, int align, out UnmanagedBuffer buffer);

            /// <summary>
            /// Освобождает неуправляемый буфер внутри сегмента
            /// </summary>
            /// <param name="buffer"></param>
            void FreeBuffer(ref UnmanagedBuffer buffer);
        }
    }
}