﻿using System;
using System.Diagnostics;

namespace JEngine.Common
{
    /// <summary>
    /// Буфер в неуправляемой памяти
    /// </summary>
    public struct UnmanagedBuffer
    {
        /// <summary>
        /// Обертка неуправляемого буфера. Выполняет освобождение буфера при вызове метода <see cref="Dispose"/>
        /// </summary>
        public class Wrapper : IDisposable
        {
            private UnmanagedBuffer _buffer;

            internal Wrapper(ref UnmanagedBuffer buffer)
            {
                _buffer = buffer;
            }

            ~Wrapper()
            {
                // Необходимо вызвать освобождение буфера в любом случае т.к. он может быть выделен через Marshal
                _buffer.Segment.FreeBuffer(ref _buffer);
                Debug.Assert(false, "Finalizer call, use Dispose!");
            }

            #region IDisposable

            private bool _isDisposed = false;

            public void Dispose()
            {
                if (!_isDisposed)
                {
                    _buffer.Segment.FreeBuffer(ref _buffer);
                    GC.SuppressFinalize(this);
                    _isDisposed = true;
                }
            }

            #endregion
        }

        /// <summary>
        /// Сегмент пула в котором был выделен буфер
        /// </summary>
        internal readonly UnmanagedMemoryPool.IPoolSegment Segment;

        /// <summary>
        /// Указатель на выделенную память (без выравнивания)
        /// </summary>
        internal readonly IntPtr RawPointer;

        /// <summary>
        /// Указатель на начало буфера
        /// </summary>
        public readonly IntPtr Pointer;

        /// <summary>
        /// Размер буфера
        /// </summary>
        public readonly int Size;

        internal UnmanagedBuffer(UnmanagedMemoryPool.IPoolSegment segment, IntPtr rawPointer, IntPtr pointer, int size)
        {
            Segment = segment;
            RawPointer = rawPointer;
            Pointer = pointer;
            Size = size;
        }

        /// <summary>
        /// Возвращает класс-обертку который реализует освобождение буфера при вызове метода <see cref="Wrapper.Dispose"/>
        /// </summary>
        /// <returns></returns>
        public Wrapper GetWrapper() => new Wrapper(ref this);
    }
}