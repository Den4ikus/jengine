using System;
using System.Diagnostics.Contracts;
using System.Threading;

namespace JEngine.Common
{
    /// <summary>
    /// Пул неуправляемой памяти
    /// </summary>
    public sealed partial class UnmanagedMemoryPool : IDisposable
    {
        private readonly StubSegment _stubSegment = new StubSegment();
        private readonly MemorySegment[] _segments;
        private readonly int _segmentSize;
        private int _segmentCount = 1;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="segmentSize">Размер сегмента пула в байтах</param>
        /// <param name="maxSegmentCount">Максимальное количество сегментов</param>
        public UnmanagedMemoryPool(int segmentSize, int maxSegmentCount)
        {
            Contract.Requires(segmentSize > 0);
            Contract.Requires(maxSegmentCount > 0);

            _segments = new MemorySegment[maxSegmentCount];
            _segments[0] = new MemorySegment(segmentSize);
            _segmentSize = segmentSize;
        }

        /// <summary>
        /// Выделяет внутри пула неуправляемый буфер заданного размера
        /// </summary>
        /// <param name="size">Размер буфера в байтах</param>
        /// <param name="align">Требуемое выравнивание буфера в байтах</param>
        /// <param name="buffer">В случае успеха содержит описание буфера, в противном случае значение не определено</param>
        /// <param name="livetime">Предполагаемое время жизни буфера</param>
        /// <returns>В случае успеха возвращается true, в противном случае false</returns>
        public bool AllocBuffer(int size, int align, out UnmanagedBuffer buffer, BufferLivetime livetime = BufferLivetime.Short)
        {
            Contract.Requires(size > 0 && align > 0);

            // Если требуется долговременный буфер выделяем через Marshal
            if (size + align > _segmentSize || livetime == BufferLivetime.Long)
                return _stubSegment.AllocBuffer(size, align, out buffer);

            // Пытаемся выделить память в существующих сегментах
            int segmentCount = _segmentCount;//Interlocked.CompareExchange(ref _segmentCount, 0, 0);
            for (int i = 0; i < segmentCount; i++)
            {
                var segment = _segments[i];//Interlocked.CompareExchange(ref _segments[i], null ,null);
                if (segment != null && segment.AllocBuffer(size, align, out buffer))
                    return true;
            }

            // В существующих сегментах не удалось выделить память
            // Пытаемся создать новый сегмент если это возможно
            do
            {
                segmentCount = Interlocked.CompareExchange(ref _segmentCount, 0, 0);
                if (segmentCount >= _segments.Length)
                {
                    // Создать новый сегмент не получилось выделяем память через Marshal
                    return _stubSegment.AllocBuffer(size, align, out buffer);
                }
            }
            while ((Interlocked.CompareExchange(ref _segmentCount, segmentCount + 1, segmentCount)) != segmentCount);

            // Удалось увеличить количество сегментов, создаем новый сегмент выделяем память и помещаем его в общий список
            var newSegment = new MemorySegment(_segmentSize);
            newSegment.AllocBuffer(size, align, out buffer);
            _segments[segmentCount] = newSegment;
            return true;
        }

        /// <summary>
        /// Освобождает неуправляемый буфер внутри пула
        /// </summary>
        /// <param name="buffer">Буфер в неуправляемой памяти пула</param>
        public void FreeBuffer(ref UnmanagedBuffer buffer)
        {
            buffer.Segment.FreeBuffer(ref buffer);
        }

        #region IDisposable

        public void Dispose()
        {
            foreach (var segment in _segments)
            {
                segment?.Dispose();
            }
            _stubSegment.Dispose();
        }

        #endregion
    }
}