using System;
using System.Diagnostics.Contracts;

namespace JEngine.Common
{
    internal static class IntPtrExt
    {
        /// <summary>
        /// Возвращает указатель выровненный на заданное количество байт
        /// </summary>
        /// <param name="ptr"></param>
        /// <param name="align">Выравнивание в байтах</param>
        /// <returns></returns>
        public static IntPtr Align(this IntPtr ptr, int align)
        {
            Contract.Requires(align > 0);
            return new IntPtr(((ptr.ToInt32() + align - 1) / align) * align);
        }
    }
}