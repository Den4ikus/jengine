﻿namespace JEngine.Common
{
    /// <summary>
    /// Время жизни буфера
    /// </summary>
    public enum BufferLivetime
    {
        /// <summary>
        /// Кратковременный буфер
        /// <para>
        /// Подходит для кратковременного размещения промежуточных данных
        /// </para> 
        /// </summary>
        Short,

        /// <summary>
        /// Долговременный буфер
        /// <para>
        /// Подходит для хранения данных связанных объектами
        /// </para>
        /// </summary>
        Long
    }
}